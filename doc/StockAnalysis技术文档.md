# StockAnalysis 技术文档

# 1 sqlalchemy 使用教程

###### 1、连接数据库

```
from sqlalchemy import create_engine

# 请替换以下内容为你的数据库连接信息
db_url = "postgresql://username:password@localhost:5432/database_name"
engine = create_engine(db_url, echo=True)  # 设置 echo=True 可以输出 SQL 语句
```



###### 2、定义数据模型

```
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)
```



###### 3、插入数据

```
from sqlalchemy.orm import sessionmaker

# 创建 Session 类
Session = sessionmaker(bind=engine)
session = Session()

# 插入数据
new_user = User(name='John Doe', age=30)
session.add(new_user)
session.commit()
```



###### 4、查询数据

```
# 查询所有用户
all_users = session.query(User).all()

# 条件查询
specific_user = session.query(User).filter_by(name='John Doe').first()
```



###### 5、更新和删除数据

```
# 更新数据
specific_user.age = 31
session.commit()

# 删除数据
session.delete(specific_user)
session.commit()
```



###### 6、使用原始 SQL 查询

```
result = engine.execute("SELECT * FROM users WHERE age > :age", {'age': 25})
for row in result:
    print(row)
```



###### 7、Session

什么时候创建Session、什么时候提交Session、什么时候关闭Session

一般来说，session在需要访问数据库的时候创建，在session访问数据库的时候，准确来说，应该是“add/update/delete”数据库的时候，会开启`database transaction`, 假设没有修改`autocommit`的默认值(False), 那么，`database transaction` 一直会保持，只有等到session rolled back, committed, or closed的时候才结束，一般建议，当`database transaction`结束的时候，同时close session, 保证，每次发起请求，都创建一个新的session



特别是对web应用来说，发起一个请求，若请求使用到Session访问数据库，则创建session，处理完这个请求后，关闭session，如下：

```
Web Server          Web Framework        SQLAlchemy ORM Code
--------------      --------------       ------------------------------
startup        ->   Web framework        # Session registry is established
                    initializes          Session = scoped_session(sessionmaker())

incoming
web request    ->   web request     ->   # The registry is *optionally*
                    starts               # called upon explicitly to create
                                         # a Session local to the thread and/or request
                                         Session()

                                         # the Session registry can otherwise
                                         # be used at any time, creating the
                                         # request-local Session() if not present,
                                         # or returning the existing one
                                         Session.query(MyClass) # ...

                                         Session.add(some_object) # ...

                                         # if data was modified, commit the
                                         # transaction
                                         Session.commit()

                    web request ends  -> # the registry is instructed to
                                         # remove the Session
                                         Session.remove()

                    sends output      <-
outgoing web    <-
response
```



还有一点需要注意的是，保证session是一个全局的对象，所以和数据库通信的session在任何时候只有一个，为毛，因为我们只需要一个session对象，同时，管理一个session对象远比管理两个对象简单



session 不是线程安全的，上面提到，我们需要保证session object是全局的，在多线程的环境中，默认情况下，多个线程将会共享同一个session， 试想一下，假设A线程正在使用session处理数据库，B线程已经执行完成，把session给close了，那么此时A在使用session就会报错，怎么避免这个问题

必须保证每个线程使用的session都不一样。

```
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

session_factory = sessionmaker(bind=some_engine)
Session = scoped_session(session_factory)
some_session = Session()
some_other_session = Session()
some_session is some_other_session #True
```



使用了`scoped_session` 默认情况下，创建的session都是`Thread-Local Scope`，创建的session对象具体有两点变化： 1 使用Session()创建的session对象都是一样的，这可以保证代码在不同的多次调用session()依然获得到相同的session 对象 2 使用Session()创建的session对象 是 `Thread-local`, session在线程与线程之间没有任何联系



怎样关闭session

```
Session.remove()

# This will first call Session.close() method on the current Session, which releases any existing transactional/connection resources still being held; transactions specifically are rolled back. The Session is then discarded. Upon next usage within the same scope, the scoped_session will produce a new Session object

# 注意，未commit的transactions会被回滚
```

那么session.close会做什么事情呢，文章开头说道，session创建和管理对数据库的连接，当调用close的时候，注意，sqlalchemy不会关闭与mysql的连接，而是把连接返回到连接池。



# 2 python常用操作

###### python对象与dict的相互转换

先举个使用场景：python处理完业务逻辑，将一个结果封装成统一格式对象，然后json格式返回给客户端。



# 3 python构建rest服务

安装flask及其相关组件

```
# 安装flask
pip install flask

# 安装flask-restful
pip install flask-restful

# 安装flasgger
# 注意：需要更新setuptools
pip install -U setuptools
pip install flasgger

# 管理数据库的依赖
pip install flask_script
pip install flask_migrate
```

