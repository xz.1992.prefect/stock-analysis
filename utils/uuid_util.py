import uuid

def generate_uuid():
    # 生成一个UUID
    new_uuid = uuid.uuid4()
    # 将UUID转换为字符串并去掉中划线
    uuid_str = str(new_uuid).replace("-", "")
    return uuid_str