# -*- coding:utf-8 -*-
from datasource import session_factory
from system.sys_logger import Logfactory


# 批量保存t_stock_k_data
def save_all(stock_info_list: []):
    if (stock_info_list is None) or (len(stock_info_list) == 0):
        Logfactory.logger.info("stock_info_list is empty")
        return

    session = session_factory.get_session()
    if session is None:
        Logfactory.logger.error("session is None")
        return

    try:
        # 批量插入
        session.bulk_save_objects(stock_info_list)
        session.commit()
        session_factory.close_session(session)
    except Exception as e:
        Logfactory.logger.exception(e)
