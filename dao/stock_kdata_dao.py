# -*- coding:utf-8 -*-
from datasource import session_factory
from system.sys_logger import Logfactory


# 批量保存t_stock_k_data
def save_all(stock_kdata_list: []):
    if (stock_kdata_list is None) or (len(stock_kdata_list) == 0):
        Logfactory.logger.info("stock_kdata_list is empty")
        return

    session = session_factory.get_session()
    if session is None:
        Logfactory.logger.error("session is None")
        return

    try:
        # 批量插入
        session.bulk_save_objects(stock_kdata_list)
        session.commit()
    except Exception as e:
        Logfactory.logger.exception(e)
        raise e
    finally:
        session_factory.close_session(session)
