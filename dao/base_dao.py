# -*- coding:utf-8 -*-
from datasource import session_factory
from system.sys_logger import Logfactory


# 批量保存t_stock_k_data
def save_all(model_list: []):
    if (model_list is None) or (len(model_list) == 0):
        Logfactory.logger.info("model_list is empty")
        return

    session = session_factory.get_session()
    if session is None:
        Logfactory.logger.error("session is None")
        return

    try:
        # 批量插入
        session.bulk_save_objects(model_list)
        session.commit()
        session_factory.close_session(session)
    except Exception as e:
        Logfactory.logger.exception(e)
