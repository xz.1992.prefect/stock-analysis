# -*- coding:utf-8 -*-
import sys

from service import stock_service, stock_kdata_service
from system.sys_logger import Logfactory


def get_stock_kdata(stock_code, start_date, end_date):
    try:
        stock_list = stock_service.select_all_stock(stock_code)
        if len(stock_list) == 0:
            Logfactory.logger.warn("No stock found . stock_code=%s, start_date=%s, end_date=%s", stock_code, start_date,
                                   end_date)
            return None
        else:
            for stock in stock_list:
                Logfactory.logger.info("get stock k-data now and stock_code=%s, start_date=%s, end_date=%s",
                                       stock.stock_code, start_date, end_date)
                code = stock.stock_code
                if code is not None and code.strip():
                    if code.startswith("sh.") or code.startswith("sz."):
                        stock_kdata_service.get_stock_k_data(stock.stock_code, start_date, end_date)
                        Logfactory.logger.info(
                            "get stock k-data success. stock_code=%s, start_date=%s, end_date=%s",
                            stock.stock_code, start_date, end_date)
                    else:
                        Logfactory.logger.info(
                            "get stock k-data now but current we don't support it . stock_code=%s, start_date=%s, end_date=%s",
                            stock.stock_code, start_date, end_date)
            Logfactory.logger.info("get all stock k-data success !!!  start_date=%s, start_date=%s", start_date,
                                   end_date)
    except Exception as e:
        Logfactory.logger.exception("get_stock_kdata exception and error message is %s we shout stop the process !!!", e)
        sys.exit()
