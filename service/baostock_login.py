# -*- coding:utf-8 -*-
"""
baostock login
"""

import baostock as bs
from system.sys_logger import Logfactory


def login_baostock():
    lg = bs.login()
    Logfactory.logger.info('login respond code:%s login_msg=%s', lg.error_code, lg.error_msg)


def logout_baostock():
    bs.logout()
    Logfactory.logger.info("login out baostock success")


def login_baostock_with_user(user_id='anonymous', password='123456'):
    login = bs.login(user_id, password)


def logout_baostock_with_user(user_id='anonymous'):
    login = bs.logout(user_id)
