# This is a sample Python script.
from system.sys_logger import Logfactory
from service import stock_kdata_service, stock_industry_info_service, stock_service, stock_kdata_business
from service import baostock_login


def get_stock_k_data():
    # 查询指定股票的时间范围K线段
    stock_kdata_service.get_stock_k_data("000155.SZ", "2024-03-01", "2024-03-08")


def get_all_stock_info():
    stock_industry_info_service.get_all_stock_info()


def get_all_stock(trade_date):
    stock_service.get_all_stock(trade_date)


def select_all_stock():
    stock_service.select_all_stock("")


def get_stock_kdata(stock_code, start_date, end_date):
    stock_kdata_business.get_stock_kdata(stock_code, start_date, end_date)


if __name__ == '__main__':
    Logfactory.logger.info('stock-analysis running now')
    # demo.login_to_baostock()
    # 000155.SZ
    # db_utils.db_connect()

    # stock_k_data_service.get_stock_k_data("000155.SZ", "2022-03-01", "2024-03-01")
    # stock_k_data_service.get_stock_sh_kdata()
    # 登录
    baostock_login.login_baostock()

    # get_all_stock_info()

    # get_all_stock("2024-03-15")
    # select_all_stock()

    # get_stock_kdata('', '1990-01-01', '2024-03-15')

    # 退出
    baostock_login.logout_baostock()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
