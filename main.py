# -*- coding:utf-8 -*-
from flask_restful import Api

from api.api_test import ApiTest
from system.sys_logger import Logfactory
from flask import Flask

app = Flask(__name__)

# 实例化一个 Api 对象，用来创建、管理 RESTful Api
api = Api(app)


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    api.add_resource(ApiTest, '/api/test')
    app.run()
    Logfactory.logger.info("StockAnalysis running now")
