import configparser as cfg
from system.sys_logger import Logfactory


def db_connect():
    config_file_path = './config/stock-analysis.properties'
    config = cfg.ConfigParser()
    config.read(config_file_path, encoding='utf-8')
    Logfactory.logger.info("config.sections() is %s", config.sections())
    Logfactory.logger.info("config[\"datasource\"][\"host\"] is %s", config["datasource"]["host"])
