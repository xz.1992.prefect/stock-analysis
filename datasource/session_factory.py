# -*- coding:utf-8 -*-
import traceback

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session, Session

from system.sys_logger import Logfactory

# 请替换以下内容为你的数据库连接信息
db_url = "postgresql://postgres:123456@127.0.0.1:5432/db_stock_analysis"
engine = create_engine(db_url, echo=True)  # 设置 echo=True 可以输出 SQL 语句


# engine = create_engine(f"postgresql+psycopg2://{db_params['user']}:{db_params['password']}@{db_params['host']}/{db_params['database']}")
def get_session():
    try:
        session_factory = sessionmaker(bind=engine)
        ScopedSession = scoped_session(session_factory)
        current_session = ScopedSession()
        Logfactory.logger.info("get session success")
        return current_session
    except Exception as e:
        Logfactory.logger.exception("get session excrption and error message is %s", e)


def close_session(session: Session):
    session.close()
    Logfactory.logger.info("close session")
