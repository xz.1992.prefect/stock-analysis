# -*- coding:utf-8 -*-
from sqlalchemy import Column, String, Numeric, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Stock(Base):
    __tablename__ = "t_stock"
    # 主键
    id = Column(String, primary_key=True)
    # 证券代码
    stock_code = Column(String)
    # 证券名称
    stock_name = Column(String)
    # 交易状态(1：正常交易 0：停牌）
    trade_status = Column(String)
