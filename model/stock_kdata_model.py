# -*- coding:utf-8 -*-

"""
Integer: 整数，映射到数据库中是 int 类型。
Float: 浮点类型，映射到数据库中是 float 类型。
Double: 双精度浮点类型，映射到数据库中是 double 类型，占据 64 位。
String: 可变字符类型，映射到数据库中是 varchar 类型。
Text: 文本类型，映射到数据库中是 text 类型，可以存储大量文本数据。
Date: 日期类型，映射到数据库中是 date 类型。
DateTime: 日期时间类型，映射到数据库中是 datetime 类型。
Boolean: 布尔类型，映射到数据库中是 boolean 类型。
"""

from sqlalchemy import Column, String, Numeric, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class StockKData(Base):
    __tablename__ = "t_stock_kdata"
    # 主键
    id = Column(String, primary_key=True)
    # 交易所行情日期
    date = Column(String)
    # 股票代码
    code = Column(String)
    # 开盘价
    open_price = Column(Numeric(10, 4))
    # 当日最高价
    high_price = Column(Numeric(10, 4))
    # 当日最低价
    low_price = Column(Numeric(10, 4))
    # 收盘价
    close_price = Column(Numeric(10, 4))
    # 前收盘价
    preclose_price = Column(Numeric(10, 4))
    # 成交量(累计单位：股)
    volume = Column(Numeric(11))
    # 成交额(单位：人民币元)
    amount = Column(Numeric(18, 6))
    # 复权状态(1：后复权， 2：前复权，3：不复权)
    adjust_flag = Column(Integer)
    # 换手率 [指定交易日的成交量(股)/指定交易日的股票的流通股总股数(股)]*100%
    turn = Column(Numeric(11, 8))
    # 交易状态(1：正常交易0：停牌)
    trade_status = Column(Integer)
    # 涨跌幅(百分比)
    pct_chg = Column(Numeric(10, 6))
    # 滚动市盈率
    pe_ttm = Column(Numeric(10, 6))
    # 市净率
    pb_mrq = Column(Numeric(10, 6))
    # 滚动市销率
    ps_ttm = Column(Numeric(10, 6))
    # 滚动市现率
    pcf_ncf_ttm = Column(Numeric(10, 6))
    # 是否ST股，1是，0否
    is_st = Column(Integer)
