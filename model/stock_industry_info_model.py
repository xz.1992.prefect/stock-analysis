# -*- coding:utf-8 -*-


from sqlalchemy import Column, String, Numeric, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

"""
股票信息
"""


class StockInfo(Base):
    __tablename__ = "t_stock_info"
    # 主键
    id = Column(String, primary_key=True)
    # A股股票代码 sh或sz.+6位数字代码，或者指数代码，如：sh.601398。sh：上海；sz：深圳。可以为空
    stock_code = Column(String)
    # 股票，证券名称
    stock_name = Column(String)
    # 所属行业
    industry = Column(String)
    # 所属行业类别, 分类
    industry_classification = Column(String)
    # 更新时间
    update_time = Column(String)
